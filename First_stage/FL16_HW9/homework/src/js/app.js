let meeting = prompt('Hello there!', "meeting"),
	values = document.querySelectorAll(".input"),
	validation = true;


if (meeting) {
	document.getElementById("main").style.display = "block";
}

let isConfirm = document.querySelector('.confirm'),
	converter = document.querySelector('.converter');

isConfirm.addEventListener('click', () => {
	validation = true;
	for (let i = 0; i < values.length; i++) {
		if (!values[i].value) {
			validation = false;
			alert('Input all data');
			break;
		}
	}
	if (!values[1].value.match(/^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/)) {
		validation = false;
		alert('Enter time in format hh:mm');
	}
	if (validation) {
		console.log(`${values[0].value} has a meeting today at ${values[1].value} somewhere in ${values[2].value}`);
	}
});

converter.addEventListener('click', () => {
	let euro = prompt('Input amount of euro'),
		dollar = prompt('Input amount of dollars'),
		dollarToUah = 27,
		euroToUah = 33;
	if (euro > 0 && dollar > 0) {
		alert(`${euro} euros are equal ${(euro * euroToUah).toFixed()} hrns,
${dollar} dollars are equal ${(dollar * dollarToUah).toFixed()} hrns`);
	}
});