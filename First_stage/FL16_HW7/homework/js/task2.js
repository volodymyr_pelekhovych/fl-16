let isPlay = confirm(`Do you want to play a game ?`);
let stages = 1;
let totalPrize = 0;
let prizeFirstAttempt = 100;
let prizeSecondAttempt = 50;
let prizeThirdAttempt = 75;

if (!isPlay) {
	alert('You did not become a billionaire, but can.');
} else {
	game();
}

function game() {
	let min = 0, max = 4 + stages * 4, attempt = 3,
		num = Math.round(Math.random() * (max - min) + min),
		choice = prompt(question(min, max, attempt, totalPrize, stages * prizeFirstAttempt));
	function makeChoice() {
		if (choice === num) {
			let prize;
			if (attempt === 3) {
				prize = prizeFirstAttempt;
			} else if (attempt === 2) {
				prize = prizeFirstAttempt - prizeSecondAttempt;
			} else if (attempt === 1) {
				prize = prizeFirstAttempt - prizeThirdAttempt;
			}
			isPlay = confirm(congrats(stages, prize));
			totalPrize += stages * prize;
			if (isPlay) {
				stages++;
				game();
			}
		} else {
			attempt--;
			if (attempt) {
				let possiblePrize;
				if (attempt === 3) {
					possiblePrize = prizeFirstAttempt;
				} else if (attempt === 2) {
					possiblePrize = prizeFirstAttempt - prizeSecondAttempt;
				} else if (attempt === 1) {
					possiblePrize = prizeFirstAttempt - prizeThirdAttempt;
				}
				choice = prompt(question(min, max, attempt, totalPrize, stages * possiblePrize));
				makeChoice();
			} else {
				alert(`Thank you for your participation. Your prize is: ${totalPrize}$`)
				isPlay = confirm('Would you like to try again?');
				if (isPlay) {
					stages = 1;
					game();
				}
			}
		}
	}
	makeChoice();
}
function question(min, max, attempt, totalPrize, prize) {
	return `Choose a number from ${min} to ${max}\nAttempts left: ${attempt}
Total prize: ${totalPrize}\nPossible prize on current attempt: ${prize}`;
}
function congrats(stage, prize) {
	return `Congratulation, you won! Your prize is ${stage * prize}$. Do you want to continue?`;
}
