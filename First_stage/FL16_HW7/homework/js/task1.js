let money = Number(prompt('Amount of your money:', 1000));
let years = Number(prompt('Number of years:', 3));
let percent = Number(prompt('Percantage of a year:', 10));

if (!Number.isInteger(years) || isNaN(money) || isNaN(percent) || money < 1000 || years < 1 || percent > 100) {
	alert('Invalid input data');
} else {
	let amount = Math.pow(1 + percent / 100, years) * money;
	let profit = amount - money;

	alert(`Initial amount: ${money}
Number of years: ${years}
Percentage of years: ${percent}

Total profit: ${profit.toFixed(2)}
Total amount: ${amount.toFixed(2)}`);
}

