const appRoot = document.getElementById('app-root');
addHeader();
function addHeader() {
	addH2();
	addTypeSearch();
	addDropDown();
}

function addH2() {
	let appHeader = document.createElement('div');
	let h2 = document.createElement('h2');
	h2.innerText = 'Countries Search';
	appHeader.appendChild(h2);
	appRoot.appendChild(appHeader);
}

function addTypeSearch() {
	let typeSearch = document.createElement('div');
	for (let i = 0; i < 3; i++) {
		let div = document.createElement('div');

		if (i === 0) {
			div.innerText = 'Please choose type of search';
		} else {
			let radioButton = document.createElement('input');
			radioButton.setAttribute('type', 'radio');
			radioButton.setAttribute('name', 'choose');
			radioButton.addEventListener('change', setSelectValues);
			let label = document.createElement('label');

			if (i === 1) {
				radioButton.setAttribute('value', 'region');
				label.innerText = 'By Region';
				label.setAttribute('for', 'region');
			}
			if (i === 2) {
				radioButton.setAttribute('value', 'languages');
				label.innerText = 'By Language';
				label.setAttribute('for', 'languages');
			}
			div.appendChild(radioButton);
			div.appendChild(label);
		}
		typeSearch.appendChild(div);
	}
	appRoot.appendChild(typeSearch);
}

function addDropDown() {
	let label = document.createElement('label');
	label.innerText = 'Please choose search query:';
	label.setAttribute('for', 'searchQyerry');

	let select = document.createElement('select');
	select.classList.add('selectValue');
	select.setAttribute('name', 'select');
	select.addEventListener('change', addTable);
	// select.addEventListener('input', addTable);     // нет возможности сразу выбрать первый пункт; надо изменить
	select.appendChild(creatOption('deafaultWriting', 'Select value'));
	appRoot.appendChild(label);
	appRoot.appendChild(select);
}

function getDataForSelect() {
	let data = {
		"regionList": externalService.getRegionsList(),
		"languagesList": externalService.getLanguagesList(),
		"countryListByLanguage": externalService.getCountryListByLanguage(),
		"countryListByRegion": externalService.getCountryListByRegion()
	};
	return data;
}

function setSelectValues() {
	let selectData = getDataForSelect();
	let select = document.querySelector('.selectValue');
	let options = select.querySelectorAll('option');
	options.forEach(el => {
		el.remove();
	})
	// this.value === 'region' || 'languages'
	selectData[`${this.value}List`].forEach(el => {
		select.appendChild(creatOption(el, el));
	})
}

function creatOption(value, innerText) {
	let option = document.createElement('option');
	option.setAttribute(`value`, `${value}`);
	option.innerText = `${innerText}`;
	return option;
}

function addTable() {
	addTableHeader();
	addTableBody(this.value);
}

function addTableHeader() {
	let tablePrev = document.querySelector('.table');
	if (tablePrev) {
		tablePrev.remove();
	}
	let table = document.createElement('table');
	table.classList.add('table');

	let tableHeader = ['Country name', 'Capital', 'World region', 'Languages', 'Area', 'Flag'];
	let firstTr = document.createElement('tr');
	for (let i = 0; i < tableHeader.length; i++) {
		let th = document.createElement('th');
		th.innerText = tableHeader[i];
		if (tableHeader[i] === 'Country name') {
			th.appendChild(createSpanArrow('nameIncrease', '&#8593;'));
			th.appendChild(createSpanArrow('nameDecrease', '&#8595;'));
		} else if (tableHeader[i] === 'Area') {
			th.appendChild(createSpanArrow('areaIncrease', '&#8593;'));
			th.appendChild(createSpanArrow('areaDecrease', '&#8595;'));
		}
		firstTr.appendChild(th);
	}
	table.appendChild(firstTr);
	appRoot.appendChild(table);
}

function createSpanArrow(className, inner) {
	let span = document.createElement('span');
	span.innerHTML = inner;
	span.classList.add(className);
	span.addEventListener('click', sort);
	return span;
}

function addTableBody(chosenSelectValue, sortCalled) {
	// console.log(sortCalled);
	let table = document.querySelector('.table');
	let tableBody = table.querySelectorAll('.infoLine');
	if (tableBody) {
		tableBody.forEach(el => {
			el.remove();
		})
	}
	let chosenCountry = [];
	if (sortCalled) {
		chosenCountry = sortCalled;
	} else {
		chosenCountry = getSortedData(chosenSelectValue);
	}
	chosenCountry.forEach(el => {
		table.appendChild(drawTableLine(el));
	})
	appRoot.appendChild(table);
}

function drawTableLine(countyArr) {
	let tableHeader = ['name', 'capital', 'region', 'languages', 'area', 'flagURL'];
	let tr = document.createElement('tr');
	tr.classList.add('infoLine');
	tableHeader.forEach((el, index) => {
		let td = document.createElement('td');
		if (el === 'languages') {
			let resString = ``;
			for (let lang in countyArr[tableHeader[index]]) {
				resString += `${countyArr[tableHeader[index]][lang]}, `;
			}
			resString = resString.split('');
			resString.length = resString.length - 2;
			resString = resString.join('');
			td.innerHTML = resString;

		} else if (el === 'flagURL') {
			let img = document.createElement('img');
			img.setAttribute('src', countyArr[tableHeader[index]]);
			img.setAttribute('alt', 'flag');
			td.appendChild(img);

		} else {
			td.innerHTML = countyArr[tableHeader[index]];
		}
		td.setAttribute('data-col', countyArr[tableHeader[index]]);
		tr.appendChild(td);
	})
	return tr;
}

function getSortedData(chosenSelectValue) {
	document.querySelectorAll('input').forEach(el => {
		if (el.checked && el.value === 'region') {
			chosenCountry = externalService.getCountryListByRegion(chosenSelectValue);
		} else if (el.checked && el.value === 'languages') {
			chosenCountry = externalService.getCountryListByLanguage(chosenSelectValue);
		}
	})
	return chosenCountry;
}

function sort() {
	let select = document.querySelector('select'),
		data = getSortedData(select.value),
		criterion = '';
	if (/name/i.test(this.className)) {
		criterion = 'name';
	} else if (/area/i.test(this.className)) {
		criterion = 'area';
	}
	if (/increase/i.test(this.className)) {
		data.sort(function (a, b) {
			if (a[criterion] > b[criterion]) {
				return 1;
			}
			if (a[criterion] < b[criterion]) {
				return -1;
			}
			return 0;
		})
	} else if (/decrease/i.test(this.className)) {
		data.sort(function (a, b) {
			if (a[criterion] < b[criterion]) {
				return 1;
			}
			if (a[criterion] > b[criterion]) {
				return -1;
			}
			return 0;
		})
	}
	addTableBody(select.value, data);
}