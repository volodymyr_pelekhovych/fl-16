function reverseNumber(num) {
	let revNumber = num;

	if (num < 0) {
		num = -num;
	}

	let str = String(num), res = '';

	for (let i = str.length - 1; i >= 0; i--) {
		res += str[i];
	}

	if (revNumber < 0) {
		return Number(-res);
	}

	return Number(res);
}

function forEach(arr, func) {
	for (let i = 0; i < arr.length; i++) {
		func(arr[i]);
	}
}

function map(arr, func) {
	let newAr = arr;
	forEach(newAr, func);

	return newAr;
}

function filter(arr, func) {
	let filteredArr = [];

	forEach(arr, (el) => {
		if (func(el)) {
			filteredArr.push(el);
		}
	})

	return filteredArr;
}

function getAdultAppleLovers(data) {
	let res = [];
	for (let index = 0; index < data.length; index++) {
		if (data[index]['favoriteFruit'] === 'apple' && data[index]['age'] >= 18) {
			res.push(data[index]['name']);
		}
	}
	return res;
}
let data = [
	{
		'_id': '5b5e3168c6bf40f2c1235cd6',
		'index': 0,
		'age': 39, 'eyeColor': 'green',
		'name': 'Stein',
		'favoriteFruit': 'apple'
	},
	{
		'_id': '5b5e3168e328c0d72e4f27d8',
		'index': 1,
		'age': 38,
		'eyeColor': 'blue',
		'name': 'Cortez',
		'favoriteFruit': 'strawberry'
	},
	{
		'_id': '5b5e3168cc79132b631c666a',
		'index': 2,
		'age': 2,
		'eyeColor': 'blue',
		'name': 'Suzette',
		'favoriteFruit': 'apple'
	},
	{
		'_id': '5b5e31682093adcc6cd0dde5',
		'index': 3,
		'age': 17,
		'eyeColor': 'green',
		'name': 'Weiss',
		'favoriteFruit': 'banana'
	}
]
getAdultAppleLovers(data)
function getKeys(obj) {
	let res = [];
	for (let key in obj) {
		res.push(key);
	}
	return res;
}

function getKeys(obj) {
	let res = [];
	for (let key in obj) {
		res.push(key);
	}
	return res;
}

function getValues(obj) {
	let res = [];
	for (let key in obj) {
		res.push(obj[key]);
	}
	return res;
}

function showFormattedDate(dateObj) {
	dateObj = '' + dateObj;
	let year = '', month = '', day = '';
	for (let i = 0; i < dateObj.length; i++) {
		if (i >= 11 && i <= 15) {
			year += dateObj[i];
		} else if (i >= 4 && i <= 7) {
			month += dateObj[i];
		} else if (i >= 8 && i <= 9) {
			day += dateObj[i];
		}
	}
	return `It is ${day} of ${month}, ${year}`;
}
