const birthday22 = new Date(2000, 9, 22);
const birthday23 = new Date(2000, 9, 23);
getAge(birthday22);
getAge(birthday23);
function getAge(birthday) {
	let today = new Date(2020, 9, 22),
		amountDaysBirthYear = birthday - new Date(birthday.getFullYear(), 0, 1),
		amountDaysCurrentYear = today - new Date(today.getFullYear(), 0, 1),
		years = today.getFullYear() - birthday.getFullYear();

	if (amountDaysCurrentYear >= amountDaysBirthYear) {
		return years;
	} else {
		years--;
		return years;
	}
}

getWeekDay(Date.now());
getWeekDay(new Date(2020, 9, 22));
function getWeekDay(day) {
	let daysNumberToName = {
		0: 'Sunday',
		1: 'Monday',
		2: 'Thuesday',
		3: 'Wendsday',
		4: 'Thursday',
		5: 'Friday',
		6: 'Saturday'
	}
	day = new Date(day);
	return daysNumberToName[day.getDay()];
}

getAmountDaysToNewYear();
getAmountDaysToNewYear();
function getAmountDaysToNewYear() {
	let today = new Date();
	today = new Date(today.getFullYear(), today.getMonth(), today.getDay());
	let newYear = new Date(today.getFullYear() + 1, 0, 1);
	let deltaSwitchTime = newYear.getTimezoneOffset() - today.getTimezoneOffset();
	return (newYear - today - deltaSwitchTime * 60 * 1000) / (1000 * 3600 * 24);
}

getProgrammersDay(2020);
getProgrammersDay(2019);
function getProgrammersDay(year) {
	let programmersDay = new Date(year, 0, 1);
	programmersDay.setDate(256);
	let daysNumberToName = ['Sunday', 'Monday', 'Thuesday', 'Wendsday', 'Thursday', 'Friday', 'Saturday'],
		monthNumberToName = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	let resString = `${programmersDay.getDate()} ${monthNumberToName[programmersDay.getMonth()]},
${programmersDay.getFullYear()} (${daysNumberToName[programmersDay.getDay()]})`;
	return resString;
}

console.log(howFarIs('friday'));
howFarIs('Thursday');
howFarIs('saturday');
function howFarIs(specifiedWeekday) {
	let today = new Date(),
		daysNumberToName = ['sunday', 'monday', 'thuesday', 'wendsday', 'thursday', 'friday', 'saturday'],
		resString = '';
	daysNumberToName.forEach((el, index) => {
		if (specifiedWeekday.toLowerCase() === el) {
			if (index === today.getDay()) {
				resString = `Hey, today is ${el} =)`;
			} else {
				let number = index - today.getDay();
				if (today.getDay() > index) {
					number = daysNumberToName.length + (index - today.getDay());
				}
				resString = `It's ${number} day(s) left till ${specifiedWeekday}`;
			}
		}
	})
	return resString;
}

isValidIdentifier('myVar!');
isValidIdentifier('myVar$');
isValidIdentifier('myVar_1');
isValidIdentifier('1_myVar');
function isValidIdentifier(variable) {
	return /^[\D]\w+[A-Za-z0-9$_]$/.test(variable);
}
const testStr = "My name is John Smith. I am 27.";
capitalize(testStr);
function capitalize(str) {
	return str.replace(/\b[a-z]/gi, symb => symb.toUpperCase());
}