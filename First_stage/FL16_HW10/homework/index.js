// Task 1
function isEquals(value1, value2) {
	return value1 === value2;
}
// Task 2
function isBigger(value1, value2) {
	return value1 > value2;
}
// Task 3
let storeNames = ('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy');
let result = storeNames.filter(word => word.length >= 1);
// Task 4
function getDifference(value1, value2) {
	if (value1 >= value2) {
		return value1 - value2;
	} else {
		return value2 - value1;
	}
}
// Task 5
const negativeCount = (array) => array.filter(array => array < 0).length;
// Task 6
function letterCount(word, letter) {
	return word.split('').reduce((counter, el) => {
		if (el === letter) {
			counter++;
		}
		return counter;
	}, 0)
}
// Task 7
function countPoints(arr) {
	return arr.reduce((counter, el) => {
		let matchResult = el.split(':');
		if (+matchResult[0] > +matchResult[1]) {
			counter += 3;
		} else if (+matchResult[0] === +matchResult[1]) {
			counter += 1;
		}
		return counter;
	}, 0)
}