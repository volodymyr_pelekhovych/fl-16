/* START TASK 1: Your code goes here */
function task1() {
	let block1 = document.querySelector('#task1'),
		table = document.createElement('table'),
		numRows = 3, numCols = 3;

	for (let i = 0; i < numRows; i++) {
		let tr = document.createElement('tr'),
			specialCellCoordRow = 1, specialCellCoordCol = 2;
		for (let k = 0; k < numCols; k++) {
			let td = document.createElement('td');
			td.setAttribute('row', i);
			td.setAttribute('col', k);
			if (i === specialCellCoordRow && k === specialCellCoordCol) {
				td.innerText = 'Special cell';
				td.addEventListener('click', toggleNotSelectedYellowColor);
			} else {
				td.innerText = 'Cell';
			}
			if (k === 0) {
				td.addEventListener('click', toggleBlueColor);
			} else {
				td.addEventListener('click', toggleYellowColor);
			}
			tr.appendChild(td);
		}
		table.appendChild(tr);
	}
	block1.appendChild(table);
	function toggleYellowColor() {
		this.classList.add('tdYellow');
	}
	function toggleBlueColor() {
		let tdArr = table.querySelectorAll('td');
		tdArr.forEach(el => {
			if (el.getAttribute('row') === this.getAttribute('row') && el.className !== 'tdYellow') {
				el.classList.add('tdBlue');

			}
		})
	}
	function toggleNotSelectedYellowColor() {
		let tdArr = table.querySelectorAll('td');
		tdArr.forEach(el => {
			if (el.className !== 'tdYellow' || el.className !== 'tdBlue') {
				el.classList.add('tdYellow');
			}
		})
	}
}
task1();
/* END TASK 1 */

/* START TASK 2: Your code goes here */
function task2() {
	let block2 = document.querySelector('#task2'),
		input = document.createElement('input'),
		button = document.createElement('button'),
		validNote = createNotifications('valid', 'Data was successfully sent'),
		invalidNote = createNotifications('invalid', 'Type number does not follow format<br>+380*********');
	input.setAttribute('placeholder', 'Type phone number in format +380*********');
	button.innerText = 'Send';
	button.setAttribute('disabled', 'true');
	input.addEventListener('change', () => validate(input, validNote, invalidNote));
	button.addEventListener('click', () => showValidMessage(validNote));
	block2.appendChild(input);
	block2.appendChild(button);
	block2.prepend(validNote);
	block2.prepend(invalidNote);
	function createNotifications(className, text) {
		let notification = document.createElement('p');
		notification.classList.add(className);
		notification.innerHTML = text;
		notification.style.display = 'none';
		return notification;
	}
	function showValidMessage(valid) {
		valid.style.display = 'block';
	}
	function validate(elem, valid, invalid) {
		if (/^\+380\d{9}$/.test(elem.value)) {
			invalid.style.display = 'none';
			input.classList.remove('invalidInput');
			button.removeAttribute('disabled');
		} else {
			valid.style.display = 'none';
			invalid.style.display = 'block';
			input.classList.add('invalidInput');
			button.setAttribute('disabled', 'true');
		}
	}
}
task2();
/* END TASK 2 */

/* START TASK 3: Your code goes here */

/* END TASK 3 */
