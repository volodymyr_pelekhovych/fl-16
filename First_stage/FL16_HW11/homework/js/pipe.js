function isFunction(functionToCheck) {
	return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}
const pipe = (value, ...funcs) => {
	return funcs.reduce((acc, el, index) => {
		if (isFunction(el)) {
			acc = el(acc);
			return acc;
		} else {
			throw `Provided argument at position ${index} is not a function!`;
		}
	}, value)
};

const replaceUnderscoreWithSpace = (value) => value.replace(/_/g, ' ');
const capitalize = (value) =>
	value
		.split(' ')
		.map((val) => val.charAt(0).toUpperCase() + val.slice(1))
		.join(' ');
const appendGreeting = (value) => `Hello, ${value}!`;

const result = pipe('john_doe', replaceUnderscoreWithSpace, capitalize, appendGreeting);
alert(result);
const error = pipe('john_doe', replaceUnderscoreWithSpace, capitalize, '');
alert(error);
