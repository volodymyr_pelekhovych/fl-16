function visitLink(path) {
	let countClick = '';
	if (!localStorage.getItem('countClick')) {
		countClick = { 'Page1': 0, 'Page2': 0, 'Page3': 0 };
	} else {
		countClick = localStorage.getItem('countClick');
		countClick = JSON.parse(countClick);
	}
	countClick[path]++;
	countClick = JSON.stringify(countClick);
	localStorage.setItem('countClick', countClick);
}
function viewResults() {
	let countClick = localStorage.getItem('countClick');
	countClick = JSON.parse(countClick);
	let ul = document.querySelector('.viewResult');
	if (ul) {
		ul.remove();
	}
	if (!localStorage.getItem('countClick')) {
		console.log(`You haven't visited any link yet`);
	} else {
		let ul = document.createElement('ul');
		ul.classList.add('viewResult');
		for (let key in countClick) {
			let li = document.createElement('li');
			li.innerText = `You visited ${key} ${countClick[key]}time(s)`;
			ul.appendChild(li);
		}
		document.querySelector('#content').appendChild(ul);
		localStorage.clear();
	}
}
